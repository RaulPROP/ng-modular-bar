import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModularBarIndicatorComponent } from './indicator.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { IndicatorDirective } from '../../directives';

describe('ModularBarIndicatorComponent Errors', () => {
	let fixture: ComponentFixture<ModularBarIndicatorComponent>;
	let component: ModularBarIndicatorComponent;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				ModularBarIndicatorComponent,
				IndicatorDirective
			]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ModularBarIndicatorComponent);
		component = fixture.componentInstance;
	});

	it('should throw an error if not giving a value', () => {
		component.maxValue = 1;
		component.minValue = 0;

		expect(() => fixture.detectChanges()).toThrowError('Value needed!');
	});

	it('should throw an error if maximum value is lower than minimum value', () => {
		component.value = 1;
		component.maxValue = 1;
		component.minValue = 2;

		expect(() => fixture.detectChanges()).toThrowError('Max value must be greater than Min value');
	});

	it('should throw an error if maximum and minimum values are equal', () => {
		component.value = 1;
		component.maxValue = 1;
		component.minValue = 1;

		expect(() => fixture.detectChanges()).toThrowError('Max and Min values must be different!');
	});

});

// TODO: Write tests
fdescribe('ModularBarIndicatorComponent', () => {
	let component: ModularBarIndicatorComponent;
	let fixture: ComponentFixture<ModularBarIndicatorComponent>;
	let indicatorDebug: DebugElement;
	let wrapperDebug: DebugElement;

	function programmaticTrigger(eventType: string, evtObj: Partial<MouseEvent>) {
		const evt = new MouseEvent(eventType, evtObj);
		document.dispatchEvent(evt);
	}

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				ModularBarIndicatorComponent,
				IndicatorDirective
			]
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent(ModularBarIndicatorComponent);
			component = fixture.componentInstance;

			component.value = 0;
			component.maxValue = 10;
			component.minValue = 0;
			component.ticks = 1;
			component.label = '';
			component.canBeModified = true;

			indicatorDebug = fixture.debugElement.query(By.css('.__mb-indicator'));
			wrapperDebug = fixture.debugElement.query(By.css('.__mb-indicator-wrapper'));

			(wrapperDebug.nativeElement as HTMLElement).getBoundingClientRect = () => {
				return {
					left: 0,
					right: 300,
					bottom: 0,
					height: 0,
					top: 0,
					width: 0,
					x: 0,
					y: 0
				};
			};

			fixture.detectChanges();
		});
	}));

	it('should create', () => {
		fixture.detectChanges();

		expect(component).toBeTruthy();
	});

	fit('should emit on new value', () => {
		indicatorDebug.triggerEventHandler('mousedown', {});

		fixture.detectChanges();
	});
});
